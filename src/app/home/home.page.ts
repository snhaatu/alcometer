import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  genders = [];
  hours = [];
  bottles = [];
  bottle: number;
  weight: number;
  gender: string;
  hour: number;
  promilles: number;
  

  constructor(private toastController: ToastController) {}

  ngOnInit() {

    this.genders.push("Male");
    this.genders.push("Female");

    for(let i = 1; i <= 24; i++) {
      this.hours.push(i);
      this.bottles.push(i);
  }    

    this.gender = "Male";
    this.hour = this.hours[0].toString();
    this.bottle = this.bottles[0].toString();
    
  }

  async calculate() {

    const litres = this.bottle * 0.33;
    let grams = litres * 8 * 4.5;
    const burning = this.weight / 10;
    grams = grams - (burning * this.hour);
    

   if(this.weight != undefined) {
    if(this.gender === "Male") {
      this.promilles = grams / (this.weight * 0.7)
    }else {
      this.promilles = grams / (this.weight * 0.6)
    }
    }else {
      const toast = await this.toastController.create({
        message: 'Weight field is not filled',
        position: 'middle',
        duration: 3000
      });
      toast.present();
    }
}

}
